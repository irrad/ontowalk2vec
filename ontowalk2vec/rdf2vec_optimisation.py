import rdflib
import numpy as np
from sklearn.utils.validation import check_is_fitted
from gensim.models.word2vec import Word2Vec
import tqdm
import copy
import pyemd
from graph import Vertex, rdflib_to_kg
from hashlib import md5


class UnknownEntityError(Exception):
    pass


class RDF2VecTransformer():
    """Project random walks or subtrees in graphs into embeddings, suited
    for classification.

    Parameters
    ----------
    vector_size: int (default: 500)
        The dimension of the embeddings.

    max_path_depth: int (default: 1)
        The maximum number of hops to take in the knowledge graph. Due to the 
        fact that we transform s -(p)-> o to s -> p -> o, this will be 
        translated to `2 * max_path_depth` hops internally.

    wl: bool (default: True)
        Whether to use Weisfeiler-Lehman embeddings

    wl_iterations: int (default: 4)
        The number of Weisfeiler-Lehman iterations. Ignored if `wl` is False.

    walks_per_graph: int (default: infinity)
        The maximum number of walks to extract from the neighborhood of
        each instance.

    n_jobs: int (default: 1)
        gensim.models.Word2Vec parameter.

    window: int (default: 5)
        gensim.models.Word2Vec parameter.

    sg: int (default: 1)
        gensim.models.Word2Vec parameter.

    max_iter: int (default: 10)
        gensim.models.Word2Vec parameter.

    negative: int (default: 25)
        gensim.models.Word2Vec parameter.

    min_count: int (default: 1)
        gensim.models.Word2Vec parameter.

    Attributes
    ----------
    model: gensim.models.Word2Vec
        The fitted Word2Vec model. Embeddings can be accessed through
        `self.model.wv.get_vector(str(instance))`.

    """
    def __init__(self, vector_size=500, max_path_depth=2, wl=True, 
                 wl_iterations=4, walks_per_graph=float('inf'), n_jobs=1, 
                 window=5, sg=1, hs= 0, alpha=0.025, max_iter=10, negative=25, min_count=1, filename="temp.csv"):
        
        self.vector_size = vector_size
        self.max_path_depth = max_path_depth
        self.wl = wl
        self.wl_iterations = wl_iterations
        self.walks_per_graph = walks_per_graph

        self.n_jobs = n_jobs
        self.window = window
        self.sg = sg
        self.hs = hs
        self.alpha = alpha
        self.max_iter = max_iter
        self.negative = negative
        self.min_count = min_count
        self.filename = filename

    def print_walks(self, walks, f):
        walk_strs = []
        for walk_nr, walk in enumerate(walks):
            s = ''
            for i in range(len(walk)):
                if i % 2:
                    s += '{} '.format(walk[i])
                else:
                    s += '{} '.format(walk[i])
                
                if i < len(walk) - 1:
                    s += '--> '

            walk_strs.append(s)

        with open(f, "w") as myfile:
            for s in walk_strs:
                myfile.write(s)
                myfile.write('\n\n')

    def _extract_random_walks(self, graph, instance):
        walks = graph.extract_random_walks(self.max_path_depth*2, instance, 
                                           max_walks=self.walks_per_graph)

        canonical_walks = set()
        for walk in walks:
            canonical_walk = []
            for i, hop in enumerate(walk):
                canonical_walk.append(hop.name)
                '''
            	canonical_walk.append(hop.name)
            	print('{} {} {}'.format(i, hop, hop.name))
                
                if i == 0:
                    canonical_walk.append(hop.name)
                else:
                    # Take the first 8 bytes of the hash, allowing for 
                    # 255**8 unique entities
                    hne = hop.name.encode()
                    #print('hop name encode - - >',hne)
                    digest = md5(hne).digest()[:8]
                    #print('digest - - >',str(digest))
                    canonical_walk.append(str(digest))
                '''
            canonical_walks.add(tuple(canonical_walk))

        return list(canonical_walks)


    def _extract_wl_walks(self, graph, instance, verbose=False):
        walks = graph.extract_random_walks(self.max_path_depth*2, instance, 
                                           max_walks=self.walks_per_graph)

        canonical_walks = set()
        for n in range(self.wl_iterations + 1):
            for walk in walks:
                canonical_walk = []
                for i, hop in enumerate(walk):
                    # For the root and predicates, we just append the name
                    if i == 0 or i % 2 == 1:
                        canonical_walk.append(hop.name)
                    # For entities, we take the Weisfeiler-Lehman label
                    else:
                        #print('wl-->', graph._label_map[hop][n])
                        canonical_walk.append(graph._label_map[hop][n])
                canonical_walks.add(tuple(canonical_walk))
        return canonical_walks

    def fit(self, graph, instances, node2vec_walks):
        """Fit the embedding network based on provided instances.
        
        Parameters
        ----------
        graphs: graph.KnowledgeGraph
            The graph from which we will extract neighborhoods for the
            provided instances. You can create a `graph.KnowledgeGraph` object
            from an `rdflib.Graph` object by using `rdflib_to_kg`.

        instances: array-like
            The instances for which an embedding will be created. It important
            to note that the test instances should be passed to the fit method
            as well. Due to RDF2Vec being unsupervised, there is no 
            label leakage.
        -------
        """
        if self.wl:
            graph.weisfeiler_lehman(iterations=self.wl_iterations)

        all_walks = []
        
        for i, instance in tqdm.tqdm(enumerate(instances)):
            if self.wl:
                #print("--------------extract wl walks-----------------")
                walks =  self._extract_wl_walks(graph, Vertex(str(instance)))
            else:
                walks =  self._extract_random_walks(graph, 
                                                    Vertex(str(instance)))
            all_walks += list(walks)
        if self.wl:
            #self.print_walks(all_walks, 'rdf2vecWL.txt')
            all_walks  += list(node2vec_walks)
        else:
            #self.print_walks(all_walks, 'rdf2vecBFS.txt')
            all_walks  += list(node2vec_walks)
        
        #self.print_walks(node2vec_walks, 'node2vec.txt')
        
        print('Extracted {} walks for {} instances!'.format(len(all_walks),
                                                            len(instances)))
        sentences = [list(map(str, x)) for x in all_walks]

        print("-------sentences-----")
        
        self.model = Word2Vec(sentences, size=self.vector_size, 
                              window=self.window, workers=self.n_jobs, 
                              sg=self.sg, iter=self.max_iter, 
                              negative=self.negative, 
                              min_count=self.min_count, alpha = self.alpha, hs = self.hs, seed=42)
        '''
        self.max_iter
        model = Word2Vec(walks, size=500, 
                              window=5, workers=1, 
                              sg=1, iter=10, 
                              negative=25, 
                              min_count=1, seed=42)
        '''

        #self.model = Word2Vec(sentences, size=128, window=10, min_count=0, sg=1, workers=8, iter=1)
        #self.model = Word2Vec(sentences, workers=8,compute_loss=True, iter = 10)
        text = str(self.window)+ ","+str(self.vector_size)+","+str(self.sg)+","+str(self.hs)+","+str(self.alpha)+","+str(self.max_iter)+","+str(self.negative)+","+str(self.min_count)+","+str(self.max_path_depth) +",,"
        training_loss = self.model.get_latest_training_loss()
        #print(training_loss)
        
        '''
        for node, v in self.model.most_similar('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_white_black_medium_left',topn=10):
        	text+= "node "+str(node)+" value "+str(v) +"\n\n"
        text += "similarity ui_style_greenBackground_white_medium_right\n"
        for node, v in self.model.most_similar('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_greenBackground_white_medium_right',topn=10):
        	text+= "node "+str(node)+" value "+str(v) +"\n\n"
        text += "similarity ui_style_yellowBackground_black_big_center \n"
        for node, v in self.model.most_similar('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_yellowBackground_black_big_center',topn=10):
        	text+= "node "+str(node)+" value "+str(v) +"\n\n"
        text += "similarity User1\n"
        for node, v in self.model.most_similar('https://gitlab.cern.ch/bgkotse/genappi/owao#User1',topn=10):
        	text+= "node "+str(node)+" value "+str(v) +"\n\n"
        text += "similarity Form1 \n"
        for node, v in self.model.most_similar('https://gitlab.cern.ch/bgkotse/genappi/owao#Form1',topn=10):
        	text+= "node "+str(node)+" value "+str(v) +"\n\n"
        '''
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_white_black_medium_left', 'https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_white_black_medium_center')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_white_black_medium_left', 'https://gitlab.cern.ch/bgkotse/genappi/owao#white_black_medium_left_Form1_pref_60')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_white_black_medium_left', 'https://gitlab.cern.ch/bgkotse/genappi/owao#User103')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_white_black_medium_left', 'https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_greenBackground_yellow_big_right')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_white_black_medium_left', 'https://gitlab.cern.ch/bgkotse/genappi/owao#User917')
        text += str(similarity)+",,"
        
        
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#User251', 'https://gitlab.cern.ch/bgkotse/genappi/owao#light_grey_black_small_left_Form0_pref_251')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#User251', 'https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_light_grey_black_small_left')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#User251', 'https://gitlab.cern.ch/bgkotse/genappi/owao#User263')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#User251', 'https://gitlab.cern.ch/bgkotse/genappi/owao#ui_style_light_grey_black_medium_left')
        text += str(similarity)+","
        similarity =  self.model.similarity('https://gitlab.cern.ch/bgkotse/genappi/owao#User251', 'https://gitlab.cern.ch/bgkotse/genappi/owao#User872')
        text += str(similarity)+",,"
        
        text += str(training_loss)+ "\n"
        
        with open(self.filename, "a") as myfile:
        	myfile.write(text)


    def transform(self, graph, instances):
        """Construct a feature vector for the provided instances.

        Parameters
        ----------
        graphs: graph.KnowledgeGraph
            The graph from which we will extract neighborhoods for the
            provided instances. You can create a `graph.KnowledgeGraph` object
            from an `rdflib.Graph` object by using `rdflib_to_kg`.

        instances: array-like
            The instances for which an embedding will be created. These 
            instances must have been passed to the fit method as well,
            or their embedding will not exist in the model vocabulary.

        Returns
        -------
        embeddings: array-like
            The embeddings of the provided instances.
        """
        check_is_fitted(self, ['model'])

        feature_vectors = []
        for instance in instances:
            feature_vectors.append(self.model.wv.get_vector(str(instance)))
        return feature_vectors


    def fit_transform(self, graph, instances, node2vec_walks):
        """First apply fit to create a Word2Vec model and then generate
        embeddings for the provided instances.

        Parameters
        ----------
        graphs: graph.KnowledgeGraph
            The graph from which we will extract neighborhoods for the
            provided instances. You can create a `graph.KnowledgeGraph` object
            from an `rdflib.Graph` object by using `rdflib_to_kg`.

        instances: array-like
            The instances for which an embedding will be created. 

        Returns
        -------
        embeddings: array-like
            The embeddings of the provided instances.
        """
        self.fit(graph, instances, node2vec_walks)
        return self.transform(graph, instances)
