import random
import os
import numpy as np

os.environ['PYTHONHASHSEED'] = '42'
random.seed(42)
np.random.seed(42)

import argparse
import rdflib
import node2vec
import pandas as pd
import matplotlib.pyplot as plt
from gensim.models import Word2Vec
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.manifold import TSNE

import matplotlib.pyplot as plt
import networkx as nx
       


from graph import rdflib_to_kg
from rdf2vec import RDF2VecTransformer

import warnings
warnings.filterwarnings('ignore')


def parse_args():
	'''
	Parses the node2vec arguments.
	'''
	parser = argparse.ArgumentParser(description="Run ontowalk2vec.")

	parser.add_argument('--input', nargs='?', default='',
	                    help='Input graph path')

	parser.add_argument('--output', nargs='?', default='emb/data.emb',
	                    help='Embeddings path')

	parser.add_argument('--dimensions', type=int, default=128,
	                    help='Number of dimensions. Default is 128.')

	parser.add_argument('--walk-length', type=int, default=80,
	                    help='Length of walk per source. Default is 80.')

	parser.add_argument('--num-walks', type=int, default=10,
	                    help='Number of walks per source. Default is 10.')

	parser.add_argument('--window-size', type=int, default=10,
                    	help='Context size for optimization. Default is 10.')

	parser.add_argument('--iter', default=1, type=int,
                      help='Number of epochs in SGD')

	parser.add_argument('--workers', type=int, default=8,
	                    help='Number of parallel workers. Default is 8.')

	parser.add_argument('--p', type=float, default=1,
	                    help='Return hyperparameter. Default is 1.')

	parser.add_argument('--q', type=float, default=1,
	                    help='Inout hyperparameter. Default is 1.')

	parser.add_argument('--weighted', dest='weighted', action='store_true',
	                    help='Boolean specifying (un)weighted. Default is unweighted.')
	parser.add_argument('--unweighted', dest='unweighted', action='store_false')
	parser.set_defaults(weighted=False)

	parser.add_argument('--directed', dest='directed', action='store_true',
	                    help='Graph is (un)directed. Default is undirected.')
	parser.add_argument('--undirected', dest='undirected', action='store_false')
	parser.set_defaults(directed=False)

	return parser.parse_args()



def main(args):

    # Load the data with rdflib
    print(end='Loading data... ', flush=True)
    g = rdflib.Graph()
    g.parse('../ontowalk2vec/data/mutag.owl')

    # Load our train & test instances and labels
    test_data = pd.read_csv('../ontowalk2vec/data/MUTAG_test.tsv', sep='\t')
    train_data = pd.read_csv('../ontowalk2vec/data/MUTAG_train.tsv', sep='\t')

    train_points = [rdflib.URIRef(x) for x in train_data['bond']]
    train_labels = train_data['label_mutagenic']

    test_points = [rdflib.URIRef(x) for x in test_data['bond']]
    test_labels = test_data['label_mutagenic']

    all_labels = list(train_labels) + list(test_labels)

    # Define the label predicates, all triples with these predicates
    # will be excluded from the graph
    label_predicates = [
        rdflib.term.URIRef('http://dl-learner.org/carcinogenesis#isMutagenic')
    ]

    # Convert the rdflib to our KnowledgeGraph object
    kg = rdflib_to_kg(g, label_predicates=label_predicates)
    

    nx_graph = nx.DiGraph()

    for v in kg._vertices:
        if not v.predicate:
            name = v.name.split('/')[-1]
            #name = v.name
            #print("name:", name)
            nx_graph.add_node(name, name=name, pred=v.predicate)
            
    for v in kg._vertices:
        if not v.predicate:
            #v_name = v.name.split('/')[-1]
            v_name = v.name
            #print("v_name:", v_name)
            # Neighbors are predicates
            for pred in kg.get_neighbors(v):
                #pred_name = pred.name.split('/')[-1]
                pred_name = pred.name
                for obj in kg.get_neighbors(pred):
                    #obj_name = obj.name.split('/')[-1]
                    obj_name = obj.name
                    nx_graph.add_edge(v_name, obj_name, name=pred_name)

    for edge in nx_graph.edges():
        nx_graph[edge[0]][edge[1]]['weight'] = 1


    G = node2vec.Graph(nx_graph, args.directed, args.p, args.q)
    G.preprocess_transition_probs()
    node2vec_walks = G.simulate_walks(args.num_walks, args.walk_length)

    #kg.visualise()
    # Create embeddings with random walks
    transformer = RDF2VecTransformer(wl=False, max_path_depth=1, sg=1)
    walk_embeddings = transformer.fit_transform(kg, train_points + test_points, node2vec_walks)

    # Create embeddings using Weisfeiler-Lehman
    
    transformer = RDF2VecTransformer(sg=1, max_path_depth=1)
    wl_embeddings = transformer.fit_transform(kg, train_points + test_points, node2vec_walks)
    

    # Fit model on the walk embeddings
    train_embeddings = walk_embeddings[:len(train_points)]
    test_embeddings = walk_embeddings[len(train_points):]

    rf =  RandomForestClassifier(random_state=42, n_estimators=100)
    rf.fit(train_embeddings, train_labels)

    accuracies = 'Accuracies\n'
    conf_matr = 'Confusion matrix \n'
    
    print('Random Forest:')
    acc1= accuracy_score(test_labels, rf.predict(test_embeddings))
    accuracies += str(acc1)+','
    print(acc1)
    cm1 = confusion_matrix(test_labels, rf.predict(test_embeddings))
    for i in range(2):
    	for j in range(2):
    		conf_matr += str(cm1[i][j]) + ','
    conf_matr += '\n'
    print(cm1)
    

    clf =  GridSearchCV(SVC(random_state=42), {'kernel': ['linear', 'poly', 'rbf'], 'C': [10**i for i in range(-3, 4)]})
    clf.fit(train_embeddings, train_labels)

    print('Support Vector Machine:')    
    acc2 = accuracy_score(test_labels, clf.predict(test_embeddings))
    accuracies += str(acc2)+','
    print(acc2)
    cm2 = confusion_matrix(test_labels, clf.predict(test_embeddings))
    for i in range(2):
    	for j in range(2):
    		conf_matr += str(cm2[i][j]) + ','
    conf_matr += '\n'
    print(cm2)
    

    # Fit model on the Weisfeiler-Lehman embeddings

    train_embeddings = wl_embeddings[:len(train_points)]
    test_embeddings = wl_embeddings[len(train_points):]

    rf =  RandomForestClassifier(random_state=42, n_estimators=100)
    rf.fit(train_embeddings, train_labels)
    

    print('Random Forest:')
    acc3 = accuracy_score(test_labels, rf.predict(test_embeddings))
    accuracies += str(acc3)+','
    print(acc3)
    cm3 = confusion_matrix(test_labels, rf.predict(test_embeddings))
    for i in range(2):
    	for j in range(2):
    		conf_matr += str(cm3[i][j]) + ','
    conf_matr += '\n'
    print(cm3)

    clf =  GridSearchCV(SVC(random_state=42), {'kernel': ['linear', 'poly', 'rbf'], 'C': [10**i for i in range(-3, 4)]})
    clf.fit(train_embeddings, train_labels)

    print('Support Vector Machine:')
    acc4 = accuracy_score(test_labels, clf.predict(test_embeddings))
    accuracies += str(acc4)+','
    cm4 = confusion_matrix(test_labels, clf.predict(test_embeddings))
    for i in range(2):
    	for j in range(2):
    		conf_matr += str(cm4[i][j]) + ','
    conf_matr += '\n'
    print(cm4)
    
    with open('results.tsv', "w") as myfile:
                myfile.write(accuracies)
                myfile.write(conf_matr)
                myfile.write('\n\n')


    # Create TSNE plots of our embeddings
    colors = ['r', 'g', 'b', 'y']
    color_map = {}
    for i, label in enumerate(set(all_labels)):
        color_map[label] = colors[i]

    f, ax = plt.subplots(1, 2, figsize=(10, 5))
    walk_tsne = TSNE(random_state=42, perplexity = 50 )
    X_walk_tsne = walk_tsne.fit_transform(walk_embeddings)

    wl_tsne = TSNE(random_state=42, perplexity = 50 )
    X_wl_tsne = wl_tsne.fit_transform(wl_embeddings)


    ax[0].scatter(X_walk_tsne[:, 0], X_walk_tsne[:, 1], c=[color_map[i] for i in all_labels])
    ax[1].scatter(X_wl_tsne[:, 0], X_wl_tsne[:, 1], c=[color_map[i] for i in all_labels])
    ax[0].set_title('Walk Embeddings')
    ax[1].set_title('Weisfeiler-Lehman Embeddings')
    plt.show()


if __name__ == "__main__":
	args = parse_args()
	main(args)
