import random
import os
import numpy as np

os.environ['PYTHONHASHSEED'] = '42'
random.seed(42)
np.random.seed(42)

import argparse
import rdflib
import node2vec
import pandas as pd
import matplotlib.pyplot as plt
from gensim.models import Word2Vec
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.manifold import TSNE

import matplotlib.pyplot as plt
import networkx as nx
       


from graph import rdflib_to_kg
from rdf2vec import RDF2VecTransformer

import warnings
warnings.filterwarnings('ignore')


def parse_args():
	'''
	Parses the node2vec arguments.
	'''
	parser = argparse.ArgumentParser(description="Run ontowalk2vec.")

	parser.add_argument('--input', nargs='?', default='',
	                    help='Input graph path')

	parser.add_argument('--output', nargs='?', default='emb/data.emb',
	                    help='Embeddings path')

	parser.add_argument('--dimensions', type=int, default=128,
	                    help='Number of dimensions. Default is 128.')

	parser.add_argument('--walk-length', type=int, default=80,
	                    help='Length of walk per source. Default is 80.')

	parser.add_argument('--num-walks', type=int, default=10,
	                    help='Number of walks per source. Default is 10.')

	parser.add_argument('--window-size', type=int, default=10,
                    	help='Context size for optimization. Default is 10.')

	parser.add_argument('--iter', default=1, type=int,
                      help='Number of epochs in SGD')

	parser.add_argument('--workers', type=int, default=8,
	                    help='Number of parallel workers. Default is 8.')

	parser.add_argument('--p', type=float, default=1,
	                    help='Return hyperparameter. Default is 1.')

	parser.add_argument('--q', type=float, default=1,
	                    help='Inout hyperparameter. Default is 1.')

	parser.add_argument('--weighted', dest='weighted', action='store_true',
	                    help='Boolean specifying (un)weighted. Default is unweighted.')
	parser.add_argument('--unweighted', dest='unweighted', action='store_false')
	parser.set_defaults(weighted=False)

	parser.add_argument('--directed', dest='directed', action='store_true',
	                    help='Graph is (un)directed. Default is undirected.')
	parser.add_argument('--undirected', dest='undirected', action='store_false')
	parser.set_defaults(directed=True)

	return parser.parse_args()

def reduce_dimensions(model):
    num_dimensions = 2  # final num dimensions (2D, 3D, etc)

    vectors = [] # positions in vector space
    labels = [] # keep track of words to label our data again later
    for word in model.wv.vocab:
        vectors.append(model.wv[word])
        labels.append(word)

    # convert both lists into numpy vectors for reduction
    vectors = np.asarray(vectors)
    labels = np.asarray(labels)

    # reduce using t-SNE
    vectors = np.asarray(vectors)
    tsne = TSNE(n_components=num_dimensions, random_state=0)
    vectors = tsne.fit_transform(vectors)

    x_vals = [v[0] for v in vectors]
    y_vals = [v[1] for v in vectors]
    return x_vals, y_vals, labels
	

def plot_with_matplotlib(x_vals, y_vals, labels, filename):
    import matplotlib.pyplot as plt
    import random

    random.seed(0)

    plt.figure(figsize=(20, 20))
    plt.scatter(x_vals, y_vals)

    #
    # Label randomly subsampled 25 data points
    #
    indices = list(range(len(labels)))
    text = "label, x, y"
    for i in indices:
    	text+= labels[i]+","+str(x_vals[i])+","+str(y_vals[i])+"\n"
    '''
    with open(filename, "w") as myfile:
        myfile.write(text)
        myfile.write('\n\n')
    '''
    selected_indices = random.sample(indices, 50)
    for i in selected_indices:
        lab = labels[i].split("#")
        if 1<len(lab):
        	plt.annotate(lab[1], (x_vals[i], y_vals[i]))
        else:
        	plt.annotate(lab[0], (x_vals[i], y_vals[i]))
    #plt.show()
    plt.savefig(filename)


def main(args, window, size, sgv, hsv, a, it, neg, count, depth, fname):

    # Load the data with rdflib
    print(end='Loading data... ', flush=True)
    g = rdflib.Graph()
    g.parse('../ontowalk2vec/data/owao.owl')
    print('OK')

    # Load our train & test instances and labels
    test_data = pd.read_csv('../ontowalk2vec/data/style_test.csv', sep=',')
    train_data = pd.read_csv('../ontowalk2vec/data/style_train.csv', sep=',')

    train_people = [rdflib.URIRef(x) for x in train_data['style']]
    train_labels = train_data['label']

    test_people = [rdflib.URIRef(x) for x in test_data['style']]
    test_labels = test_data['label']

    all_labels = list(train_labels) + list(test_labels)

    # Define the label predicates, all triples with these predicates
    # will be excluded from the graph
    label_predicates = [
        rdflib.term.URIRef('http://gitlab.cern.ch/bgkotse/genappi/owao#popular')
    ]

    # Convert the rdflib to our KnowledgeGraph object
    kg = rdflib_to_kg(g, label_predicates=label_predicates)
    

    nx_graph = nx.DiGraph()

    for v in kg._vertices:
        if not v.predicate:
            #name = v.name.split('/')[-1]
            name = v.name
            #print("name:", name)
            nx_graph.add_node(name, name=name, pred=v.predicate)
            
    for v in kg._vertices:
        if not v.predicate:
            #v_name = v.name.split('/')[-1]
            v_name = v.name
            #print("v_name:", v_name)
            # Neighbors are predicates
            for pred in kg.get_neighbors(v):
                #pred_name = pred.name.split('/')[-1]
                pred_name = pred.name
                for obj in kg.get_neighbors(pred):
                    #obj_name = obj.name.split('/')[-1]
                    obj_name = obj.name
                    nx_graph.add_edge(v_name, obj_name, name=pred_name)

    for edge in nx_graph.edges():
        nx_graph[edge[0]][edge[1]]['weight'] = 1


    G = node2vec.Graph(nx_graph, args.directed, args.p, args.q)
    G.preprocess_transition_probs()
    
    
    node2vec_walks = G.simulate_walks(args.num_walks, args.walk_length)

    #kg.visualise()
    # Create embeddings with random walks
    #transformer = RDF2VecTransformer(wl=False, max_path_depth=1, sg=1)
    bfsfname =  "/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/results_bfs.csv"
    transformer = RDF2VecTransformer(wl = False, vector_size= size, max_path_depth = depth, window = window, sg = sgv, hs = hsv, alpha = a, max_iter = it, negative = neg, min_count = count, filename= bfsfname)
    walk_embeddings = transformer.fit_transform(kg, train_people + test_people, node2vec_walks)
    walk_model =  transformer.model

    # Create embeddings using Weisfeiler-Lehman
    #transformer = RDF2VecTransformer(sg=1, max_path_depth=1)
    wlfname =  "/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/results_wl.csv"
    transformer = RDF2VecTransformer(wl = True, vector_size= size, max_path_depth = depth, window = window, sg = sgv, hs = hsv, alpha = a, max_iter = it, negative = neg, min_count = count, filename = wlfname)
    wl_embeddings = transformer.fit_transform(kg, train_people + test_people, node2vec_walks)
    wl_model =  transformer.model
    
    

    # Fit model on the walk embeddings
    train_embeddings = walk_embeddings[:len(train_people)]
    test_embeddings = walk_embeddings[len(train_people):]

    rf =  RandomForestClassifier(random_state=42, n_estimators=100)
    rf.fit(train_embeddings, train_labels)
    accuracy_results = ""
    confusion_results = ""

    #print('Random Forest:')
    accuracy = accuracy_score(test_labels, rf.predict(test_embeddings))
    accuracy_results+=str(accuracy)+","
    #print(accuracy)
    confusion = confusion_matrix(test_labels, rf.predict(test_embeddings))
    for i in confusion:
    	for j in i:
    		confusion_results+=str(j)+","	
    	confusion_results+="\n"
    confusion_results+="\n"
    #print(confusion)

    clf =  GridSearchCV(SVC(random_state=42), {'kernel': ['linear', 'poly', 'rbf'], 'C': [10**i for i in range(-3, 4)]})
    clf.fit(train_embeddings, train_labels)

    #print('Support Vector Machine:')
    accuracy = accuracy_score(test_labels, clf.predict(test_embeddings))
    accuracy_results+=str(accuracy)+","
    #print(accuracy)
    confusion = confusion_matrix(test_labels, clf.predict(test_embeddings))
    for i in confusion:
    	for j in i:
    		confusion_results+=str(j)+","	
    	confusion_results+="\n"
    confusion_results+="\n"
    #print(confusion)

    # Fit model on the Weisfeiler-Lehman embeddings

    train_embeddings = wl_embeddings[:len(train_people)]
    test_embeddings = wl_embeddings[len(train_people):]

    rf =  RandomForestClassifier(random_state=42, n_estimators=100)
    rf.fit(train_embeddings, train_labels)

    #print('Random Forest:')
    accuracy=accuracy_score(test_labels, rf.predict(test_embeddings))
    accuracy_results+=str(accuracy)+","
    #print(accuracy)
    confusion = confusion_matrix(test_labels, rf.predict(test_embeddings))
    for i in confusion:
    	for j in i:
    		confusion_results+=str(j)+","	
    	confusion_results+="\n"
    confusion_results+="\n"
    #print(confusion)

    clf =  GridSearchCV(SVC(random_state=42), {'kernel': ['linear', 'poly', 'rbf'], 'C': [10**i for i in range(-3, 4)]})
    clf.fit(train_embeddings, train_labels)

    #print('Support Vector Machine:')
    accuracy = accuracy_score(test_labels, clf.predict(test_embeddings))
    accuracy_results+=str(accuracy)+"\n"
    #print(accuracy)
    confusion = confusion_matrix(test_labels, clf.predict(test_embeddings))
    for i in confusion:
    	for j in i:
    		confusion_results+=str(j)+","	
    	confusion_results+="\n"
    confusion_results+="\n"
    #print(confusion)


    # Create TSNE plots of our embeddings
    #colors = ['r','g', 'b', 'y']
    '''
    colors = ['g', 'b', 'y','r']
    color_map = {}
    for i, label in enumerate(set(all_labels)):
        color_map[label] = colors[i]
        print('label->', label)
        print('colors->', colors[i])

    f, ax = plt.subplots(1, 2, figsize=(10, 5))
    walk_tsne = TSNE(random_state=42)
    X_walk_tsne = walk_tsne.fit_transform(walk_embeddings)

    wl_tsne = TSNE(random_state=42)
    X_wl_tsne = wl_tsne.fit_transform(wl_embeddings)


    ax[0].scatter(X_walk_tsne[:, 0], X_walk_tsne[:, 1], c=[color_map[i] for i in all_labels])
    ax[1].scatter(X_wl_tsne[:, 0], X_wl_tsne[:, 1], c=[color_map[i] for i in all_labels])
    ax[0].set_title('Walk Embeddings')
    ax[1].set_title('Weisfeiler-Lehman Embeddings')
    plt.savefig("/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/"+fname+".png")
    
    
    x_vals, y_vals, labels = reduce_dimensions(walk_model)
    plot_with_matplotlib(x_vals, y_vals, labels,"/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/"+fname+"_bfs.png")
    x_vals, y_vals, labels = reduce_dimensions(wl_model)
    plot_with_matplotlib(x_vals, y_vals, labels,"/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/"+fname+"_wl.png")
    '''

    return (accuracy_results, confusion_results)

if __name__ == "__main__":
	args = parse_args()
	acc = ""
	conf = ""
	
	'''
	windows = [3,5,7,10,15]
	sizes = [20,50,100, 200, 500]
	sg_values = [0, 1]
	hs_values = [0, 1]
	alpha_values = [0.5, 0.1, 0.05,0.025,0.01, 0.005]
	iterations = [1,2,5,10,20]
	negative = [0,1,5,10,15,20,25,50]
	min_counts = [0,1,2,3,4,5,10]
	max_path_depths = [1]
	'''
	windows = [3,5,7]
	sizes = [20,100, 200, 500]
	sg_values = [1]
	hs_values = [0] #hierarchical softmax, no need for negative
	alpha_values = [0.1, 0.05,0.025,0.01]
	iterations = [1,5,20,50]
	negative = [0,1,5,10,20]
	min_counts = [0,1]
	max_path_depths = [1,2,3]
	bfsfname =  "/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/results_bfs.csv"
	wlfname =  "/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/results_wl.csv"
	titles = "window,size,sg,hs,alpha,iterations,negative,counts,depth,,ui_style_white_black_medium_center,white_black_medium_left_Form1_pref_60,User103,ui_style_greenBackground_yellow_big_right,User917,,light_grey_black_small_left_Form0_pref_251,ui_style_light_grey_black_small_left,User263,ui_style_light_grey_black_medium_left,User872,,loss\n"
	with open(bfsfname, "w") as bfsf:
		bfsf.write(titles)
	with open(wlfname, "w") as wlf:
		wlf.write(titles)		
	for depth in max_path_depths:
		for count in min_counts:
			for neg in negative:
				for it in iterations:
					for a in alpha_values:
						for sgv in sg_values:
							for size in sizes:
								for window in windows:
									if neg == 0:
										hsv = 1
									else:
										hsv = 0
									folder ="window_"+str(window)+'_size_'+str(size)+"_sg_"+ str(sgv)+"_hs_"+ str(hsv)+"_a_"+str(a)+"_it_"+str(it)+"_neg"+str(neg)+"_count_"+str(count)+"_depth_"+str(depth)
									print(folder)
									#os.mkdir("/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/"+folder)
									acc=""
									conf = ""
									for i in range(1):
										fname =  folder+"/"+folder+"_"+str(i)
										accuracy, confusion=main(args, window, size, sgv, hsv, a, it, neg, count, depth, fname)
										acc += accuracy
										conf+="i="+str(i)+"\n"
										conf+= confusion
										with open("/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/accuracy_results.csv", "a") as myfile:
											myfile.write(folder)
											myfile.write(',')
											myfile.write(acc)
											myfile.write('\n')
										with open("/home/ina/Documents/PhD/embeddings/ontowalk2vec/benchmark_results/confusion_matrix_results.csv", "a") as cfile:
											cfile.write(folder)
											cfile.write('\n')
											cfile.write(conf)
											cfile.write('\n\n')
		
